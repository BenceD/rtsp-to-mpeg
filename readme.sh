Download ffmpeg and ffserver and put it in this folder
https://ffbinaries.com/downloads

run start.sh

STREAM IS HERE:
http://localhost:8090/stream.mjpg


Source:
https://forum.videolan.org/viewtopic.php?t=127984
https://superuser.com/questions/348634/install-ffserver-on-mac-os-x-10-6


# Run as a Service:
## Create the Service file called rtsp.service
```
[Unit]
Description=RTSP_Streamer

[Service]
ExecStart=/home/blackswan/RTSP-Streamer/start.sh

[Install]
WantedBy=multi-user.target
```

## Place it in /lib/systemd/system folder

## Make sure that your script executable with:
```
chmod u+x /home/blackswan/RTSP-Streamer/start.sh
```

Start the service:
```
sudo systemctl start rtsp
```

Enable it to run at boot:
```
sudo systemctl enable rtsp
```

Stop it:
```
sudo systemctl stop rtsp
```

Restart:
```
sudo systemctl restart rtsp
```



## Links:
The mjpeg webcam stream is available on this link: http://10.36.2.250:8090/stream.mjpg
The jpg snapshot is available on this link: http://10.36.2.250:8090/printerfoto.jpg
